package com.masterpass.merchantonboarding.ui.entries;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.masterpass.merchantonboarding.R;
import com.masterpass.merchantonboarding.databinding.GettingStartedFragmentBinding;
import com.masterpass.merchantonboarding.ui.personalinfo.PersonalInfoActivity;

public class GettingStartedFragment extends Fragment {

    public static GettingStartedFragment newInstance() {
        return new GettingStartedFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        GettingStartedFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.getting_started_fragment, container, false);
        GettingStartedViewModel gettingStartedViewModel = ViewModelProviders.of(this).get(GettingStartedViewModel.class);
        binding.setViewModel(gettingStartedViewModel);

        gettingStartedViewModel.onGetStartedPressedEvent.observe(this, o -> launchPersonalInfoScreen());
        return binding.getRoot();
    }


    private void launchPersonalInfoScreen() {
        requireActivity().startActivity(new Intent(requireActivity(), PersonalInfoActivity.class));
    }
}
