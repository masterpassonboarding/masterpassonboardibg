package com.masterpass.merchantonboarding.ui.entries;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.masterpass.merchantonboarding.R;
import com.masterpass.merchantonboarding.databinding.EntriesFragmentBinding;

public class EntriesFragment extends Fragment {

    private EntriesViewModel mViewModel;

    public static EntriesFragment newInstance() {
        return new EntriesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         EntriesFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.entries_fragment, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(EntriesViewModel.class);

        mViewModel.screenTimerDoneLiveData().observe(this, isScreenShowingDone -> launchGetStartedScreen());
    }

    private void launchGetStartedScreen() {
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, GettingStartedFragment.newInstance())
                .commitNow();
    }

}
