package com.masterpass.merchantonboarding.ui.entries;

import android.arch.lifecycle.ViewModel;

import com.masterpass.merchantonboarding.ui.SingleLiveData;

public class GettingStartedViewModel extends ViewModel {

    SingleLiveData onGetStartedPressedEvent = new SingleLiveData<Void>();


    public void onGetStartedPressed() {
        onGetStartedPressedEvent.call();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}
