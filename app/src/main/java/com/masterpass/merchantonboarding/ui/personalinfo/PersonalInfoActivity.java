package com.masterpass.merchantonboarding.ui.personalinfo;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.masterpass.merchantonboarding.R;

public class PersonalInfoActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        DataBindingUtil.setContentView(this, R.layout.personalinfo_activity);
    }
}
