package com.masterpass.merchantonboarding.ui.entries;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.masterpass.merchantonboarding.R;
import com.masterpass.merchantonboarding.databinding.EntriesActivityBinding;
import com.masterpass.merchantonboarding.ui.entries.EntriesFragment;

public class EntriesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.entries_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, EntriesFragment.newInstance())
                    .commitNow();
        }
    }
}
