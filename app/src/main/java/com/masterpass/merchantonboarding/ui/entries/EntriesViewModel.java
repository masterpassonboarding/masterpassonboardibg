package com.masterpass.merchantonboarding.ui.entries;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class EntriesViewModel extends ViewModel {

    private MutableLiveData<Boolean> mutableScreenTimeLiveData;

    LiveData<Boolean> screenTimerDoneLiveData() {
        if (mutableScreenTimeLiveData == null) {
            mutableScreenTimeLiveData = new MutableLiveData<>();
            loadScreen();
        }

        return mutableScreenTimeLiveData;
    }

    /**
     * handler will call the live data event on ui after 5 sec. that is being observed by fragment.
     */
    private void loadScreen() {

        android.os.Handler handler = new android.os.Handler();

        handler.postDelayed(() -> {
            mutableScreenTimeLiveData.setValue(true);
        }, 5000);

    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}
